print('Конвертер pdf для Bosch version 2.2.1 (Лютов МА, maxim_7@mail.ru)\n')

from os import path
from openpyxl import Workbook
from progress.bar import IncrementalBar
import fitz, sys, re

wb = Workbook()
ws = wb.active

if len(sys.argv) > 1:
    path_to_file = sys.argv[1]
    print(f"Обрабатываем {path_to_file}")
else:
    sys.exit(f" --- Справка ---:\nКоманда: [скрипт] [файл pdf]\n")

head = [
'Артикул',
'Bosch',
'Кол-во',
'Сумма налога',
'Стоимость товаров',
'Страна происхождения',
'Регистрационный номер',]

ws.append(head)

patterns = ['\n\d+\n([A-z\d]{9,})\n*()[-+(.),\w\n\s\[\]/]*(?:ШТ|НАБОР)\n(\d+)[.,\d\s\w]*%\n([\s,.\d]+?)\n([\s,.\d]+?)\n[-\d]*\n(.*[\w\n-]*)\n([/\d]*\n\d*|.*)',
           ]
tmpl = []
string = ''
count_str = 0

for pattern in patterns:
    tmpl.append(re.compile(pattern))

with fitz.open(path_to_file) as data_pdf:    
    bar = IncrementalBar('Страниц обработано:', max = len(data_pdf))
    for page in range(len(data_pdf)):
       string += data_pdf.load_page(page).get_text("text")
       bar.next()

    matchs=[]
    for i in range(len(patterns)):
        matchs.append(tmpl[i].findall(string))
    
    count_str = list(zip(*matchs))
    for match in count_str:
        ws.append(list(map(lambda x: x.replace("\n",''), list(*match))))
    
total_file = path.basename(path_to_file).replace('.','_')
wb['Sheet'].title = total_file

try:
    wb.save(path.basename(path_to_file).replace('.','_') + '.xlsx')
except Exception as ex:
    print(f"\n{ex}")
else:
    print(f"\nРаспознано строк: {len(count_str)}\nСоздан: {total_file + '.xlsx'}")
